#!/bin/sh
VER="R0.07"
# R0.07/lejo 2017-01-20 Add -readmebasedir to the README files from the manifest instead of s_targets

BBTEMPLATE=
BBXML=
BB_TARGETREADME_BASE=

USAGE="`basename $0` -xml buildbootxml-to-create  -template templatexml-file -readmebasedir targetreadmebase ($VER)
    Currently only supports sequence types Build-command: and Boot-command:
    Both files should have path book-*release-info/doc/
    Creates the XML file from the template, inserting build/boot commands
    from the various s_manifests/el_manifest-XXX/XXXtargetXXX/README files
    at the place in template with >SCRIPT_INCLUDES_BUILD_BOOT_SECTIONS_HERE<
    ignoring rest of template
    The code tries to fold too long lines, but this is not perfect. Best would
    be if the command lines already in README are short enough, e.g. by
    using short variables, which work both on shell and uboot command lines"

while echo "x$1" | egrep '^x-' >/dev/null 2>&1
do
  OPT="$1" ; shift
  if [ "$OPT" = "--help" -o "$OPT" = "-h" -o "$OPT" = "-help" ] ; then echo "$USAGE" ; exit ; fi
  if [ "$OPT" = "-xml" ] ; then BBXML="$1" ; shift; fi
  if [ "$OPT" = "-template" ] ; then BBTEMPLATE="$1" ; shift; fi
  if [ "$OPT" = "-readmebasedir" ] ; then BB_TARGETREADME_BASE="$1" ; shift; fi
done
if [ "$BBTEMPLATE" = "" ]; then echo "ERROR: Missing option -template templatefile"; exit ; fi
if [ "$BBXML" = ""      ]; then echo "ERROR: Missing option -xml buildbootxml-to-create"; exit ; fi
if [ ! -f "$BBTEMPLATE" ]; then echo "ERROR: Missing templatefile '$BBTEMPLATE'"; exit; fi
if [ ! -d "`dirname \"$BBXML\"`" ]; then echo "ERROR: Missing parent directory for '$BBXML'"; exit ; fi
if [ ! -d "$BB_TARGETREADME_BASE" ]; then echo "ERROR: Missing basedir for README files '$BB_TARGETREADME_BASE'"; exit; fi

echo "`basename $0` Creating $BBXML from"
TARGETREADMES=`cd $BB_TARGETREADME_BASE ; ls -d */README | tr '\n' ' '`
echo "     $TARGETREADMES"

# README file formats:
# a) Sequence starts: ___ XXXX:yyyy or ___ XXXX:yyyy conffile
#    where XXXX is a type, yyyy is text to be in title
# b) Inside sequence: ___ END ends the sequence  (ignore rest of line)
# c) Inside sequence: # Documentation line
# d) Inside sequence: Anything else is command or config lines
#    Conv.to XML: ">" "<" "&" and put all inside <programlisting>
# *) Anywhere ____xxxx Leading 4 underlines or more, always ignored
#    unless one of the recognized XXXX
# *) Anywhere outside sequence, ignore all
# *) There can be multiple of each type of sequence in each README file
#    with different yyyy


cat $BBTEMPLATE | awk '
   />SCRIPT_INCLUDES_BUILD_BOOT_SECTIONS_HERE</ {exit 0; }
   { print $0; }
' >$BBXML


# Long command lines: The awk code below breaks too long lines, but this is not perfect.
extractcmds_filter() {
   echo "        <programlisting>" | tr -d '\n'
   sed '/^___/d;s/\&/\&amp;/g' | sed 's/</\&lt;/g;s/>/\&gt;/g;/^$/d' | \
    awk 'BEGIN               { MAX=90; }
         ( length($0) > MAX ) {
            LINE=$0;
            while (length(LINE) > MAX) {
              if (index(LINE," ") == 0 ) {
                print "ERROR: PROBLEM: No space in too long line:" LINE  > "/dev/stderr";
                print $LINE;
                next;
              }
              i=MAX; while ( substr(LINE,i,1) != " " ) { i=i-1; if (i==0) {break;} }
              print substr(LINE,0,i) "\\";
              REST=substr(LINE,i+1);
              if ( length(REST) == 0 ) { next ; }
              LINE="           " REST;
            }
            if ( length(LINE) > 0 ) { print LINE; next ; }
         }
         { print;}'
   echo "</programlisting>"
}

extractcmds_for_type() { # target/README BOOTorBUILD
  README=$BB_TARGETREADME_BASE/"$1"
  CMDTYPE="$2"
  COMMANDSFOR=`egrep "___$CMDTYPE:" $README`
  for CMDS in $COMMANDSFOR
  do
     cmdsfor=`echo "$CMDS" | sed 's/[^:]*://'`
#--      echo "         <para>$CMDTYPE for $cmdsfor</para>"
     cat "$README" | sed -n "/$COMMANDSFOR/,/___END/p" | extractcmds_filter
  done
}

for targetreadme in $TARGETREADMES
do
   TARGET=`dirname $targetreadme`
   echo "" >>$BBXML
   echo "  <section id=\"target_$TARGET\">" >>$BBXML
   echo "    <title>Target $TARGET</title>" >>$BBXML
   echo "     <remark>NOTE: DO NOT EDIT THIS GENERATED FILE! Only edit the template file.</remark>" >>$BBXML
   echo "    <section>" >>$BBXML
   echo "        <title>Build Instructions for $TARGET</title>" >>$BBXML
   extractcmds_for_type $targetreadme Build-command >>$BBXML
   echo "    </section>" >>$BBXML
   echo "" >>$BBXML
   echo "    <section>" >>$BBXML
   echo "       <title>Boot Instructions for $TARGET</title>" >>$BBXML
   extractcmds_for_type $targetreadme Boot-command >>$BBXML
   echo "    </section>" >>$BBXML
   echo "  </section>" >>$BBXML
done

echo "</chapter>" >>$BBXML
echo "Ready created $BBXML"
