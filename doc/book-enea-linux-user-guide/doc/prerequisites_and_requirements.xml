<?xml version="1.0" encoding="ISO-8859-1"?>
<chapter id="prereq_reqs">
  <title>Prerequisites and Requirements</title>

  <para>Building Enea Linux or compiling applications requires that your git
  environment be setup properly and for certain packages to be installed on
  your Linux development host. The following chapter details the
  configurations needed on the build environment in order to properly use Enea
  Linux.</para>

  <section id="gitconfig">
    <title>Git Configuration</title>

    <para>If you intend to get Enea Linux sources and build Enea Linux
    yourself, you will need Git installed in your build environemtn. Please
    refer to <ulink
    url="https://git-scm.com/book/en/v2/Getting-Started-First-Time-Git-Setup">Getting
    Started - First-Time Git Setup</ulink>, for more details on how to set up
    your git environment correctly, including how to set your identity using
    the following commands:</para>

    <programlisting>$ git config --global user.name "John Doe"
$ git config --global user.email johndoe@example.com</programlisting>
  </section>

  <section id="hostpackages">
    <title>Host Packages</title>

    <para>In order to work with Enea Linux, you need a set of tools installed
    on the build machine, depending on the scenario you use. The following
    chapters will describe what tools to install on the build machine.</para>

    <section id="prebuiltprereq">
      <title>Using Pre-Build Binaries</title>

      <para>Using the pre-built binaries, you can get up and running more
      quickly. Since building is not required, there are not a lot of packaes
      and tools that need to be installed but a few are still required:</para>

      <itemizedlist>
        <listitem>
          <para>wget - for downloading the Enea Linux binaries</para>
        </listitem>

        <listitem>
          <para>tar - for decompressing the Enea Linux release</para>
        </listitem>

        <listitem>
          <para>tftpboot server - for deploying Enea Linux on target</para>
        </listitem>

        <listitem>
          <para>NFS server - in case you want to mount the root file system
          over NFS</para>
        </listitem>
      </itemizedlist>
    </section>

    <section id="hostprereqpackages">
      <title>Required Packages for the Host Development System</title>

      <para>Building Enea Linux requires a set of packages to be installed on
      your Linux development host. The list of required packages is described
      in the <ulink
      url="https://www.yoctoproject.org/docs/2.7/ref-manual/ref-manual.html#required-packages-for-the-host-development-system">Yocto
      Project reference manual</ulink>.</para>
    </section>
  </section>

  <section id="sysshell_config">
    <title>Default Shell Configuration</title>

    <para>Before installing Enea Linux, make sure that
    <filename>bash</filename> is the default shell.</para>

    <para><emphasis role="bold">To verify the default system
    shell</emphasis></para>

    <itemizedlist>
      <listitem>
        <para>If your system runs Ubuntu, use list to verify if
        <filename>/usr/bin</filename> is a symbolic link to <emphasis
        role="bold">bash</emphasis>:</para>

        <programlisting># ls -l /bin/sh
lrwxrwxrwx 1 root root 4 2012-03-02 11:53 /bin/sh -&gt; bash</programlisting>
      </listitem>

      <listitem>
        <para>Optionally, in case the link points to <literal>dash</literal>,
        change it through the following steps:</para>

        <programlisting># ls -l /bin/sh
lrwxrwxrwx 1 root root 4 2012-03-02 11:53 /bin/sh -&gt; dash
# sudo dpkg-reconfigure dash
Use dash as the default system shell (/bin/sh)? No</programlisting>
      </listitem>
    </itemizedlist>
  </section>
</chapter>
